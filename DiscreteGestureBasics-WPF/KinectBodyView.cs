﻿//------------------------------------------------------------------------------
// <copyright file="KinectBodyView.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Microsoft.Samples.Kinect.DiscreteGestureBasics
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading;
    using System.Windows;
    using System.Windows.Media;
    using Microsoft.Kinect;

    using System.Net.Sockets;
    using System.Net;
    using LightBuzz.Vitruvius;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;

    /// <summary>
    /// Visualizes the Kinect Body stream for display in the UI
    /// </summary>
    public sealed class KinectBodyView
    {
        /// <summary>
        /// Radius of drawn hand circles
        /// </summary>
        private const double HandSize = 30;

        /// <summary>
        /// Thickness of drawn joint lines
        /// </summary>
        private const double JointThickness = 3;

        /// <summary>
        /// Thickness of clip edge rectangles
        /// </summary>
        private const double ClipBoundsThickness = 10;

        /// <summary>
        /// Constant for clamping Z values of camera space points from being negative
        /// </summary>
        private const float InferredZPositionClamp = 0.1f;

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as closed
        /// </summary>
        private readonly Brush handClosedBrush = new SolidColorBrush(Color.FromArgb(128, 255, 0, 0));

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as opened
        /// </summary>
        private readonly Brush handOpenBrush = new SolidColorBrush(Color.FromArgb(128, 0, 255, 0));

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as in lasso (pointer) position
        /// </summary>
        private readonly Brush handLassoBrush = new SolidColorBrush(Color.FromArgb(128, 0, 0, 255));

        /// <summary>
        /// Brush used for drawing joints that are currently tracked
        /// </summary>
        private readonly Brush trackedJointBrush = new SolidColorBrush(Color.FromArgb(255, 68, 192, 68));

        /// <summary>
        /// Brush used for drawing joints that are currently inferred
        /// </summary>        
        private readonly Brush inferredJointBrush = Brushes.Yellow;

        /// <summary>
        /// Pen used for drawing bones that are currently inferred
        /// </summary>        
        private readonly Pen inferredBonePen = new Pen(Brushes.Gray, 1);

        /// <summary>
        /// Drawing group for body rendering output
        /// </summary>
        private DrawingGroup drawingGroup;

        /// <summary>
        /// Drawing image that we will display
        /// </summary>
        private DrawingImage imageSource;

        /// <summary>
        /// Coordinate mapper to map one type of point to another
        /// </summary>
        private CoordinateMapper coordinateMapper = null;

        /// <summary>
        /// definition of bones
        /// </summary>
        private List<Tuple<JointType, JointType>> bones;

        /// <summary>
        /// Width of display (depth space)
        /// </summary>
        private int displayWidth;

        /// <summary>
        /// Height of display (depth space)
        /// </summary>
        private int displayHeight;

        /// <summary>
        /// List of colors for each body tracked
        /// </summary>
        private List<Pen> bodyColors;

        private double timeDifference;

        /// <summary>
        /// Initializes a new instance of the KinectBodyView class
        /// </summary>
        /// <param name="kinectSensor">Active instance of the KinectSensor</param>
        /// 


        public KinectBodyView(KinectSensor kinectSensor)
        {
            if (kinectSensor == null)
            {
                throw new ArgumentNullException("kinectSensor");
            }

            // get the coordinate mapper
            this.coordinateMapper = kinectSensor.CoordinateMapper;

            // get the depth (display) extents
            FrameDescription frameDescription = kinectSensor.DepthFrameSource.FrameDescription;

            // get size of joint space
            this.displayWidth = frameDescription.Width;
            this.displayHeight = frameDescription.Height;

            // a bone defined as a line between two joints
            this.bones = new List<Tuple<JointType, JointType>>();

            // Torso
            this.bones.Add(new Tuple<JointType, JointType>(JointType.Head, JointType.Neck));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.Neck, JointType.SpineShoulder));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineShoulder, JointType.SpineMid));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineMid, JointType.SpineBase));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineShoulder, JointType.ShoulderRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineShoulder, JointType.ShoulderLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineBase, JointType.HipRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineBase, JointType.HipLeft));

            // Right Arm
            this.bones.Add(new Tuple<JointType, JointType>(JointType.ShoulderRight, JointType.ElbowRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.ElbowRight, JointType.WristRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.WristRight, JointType.HandRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.HandRight, JointType.HandTipRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.WristRight, JointType.ThumbRight));

            // Left Arm
            this.bones.Add(new Tuple<JointType, JointType>(JointType.ShoulderLeft, JointType.ElbowLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.ElbowLeft, JointType.WristLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.WristLeft, JointType.HandLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.HandLeft, JointType.HandTipLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.WristLeft, JointType.ThumbLeft));

            // Right Leg
            this.bones.Add(new Tuple<JointType, JointType>(JointType.HipRight, JointType.KneeRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.KneeRight, JointType.AnkleRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.AnkleRight, JointType.FootRight));

            // Left Leg
            this.bones.Add(new Tuple<JointType, JointType>(JointType.HipLeft, JointType.KneeLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.KneeLeft, JointType.AnkleLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.AnkleLeft, JointType.FootLeft));

            // populate body colors, one for each BodyIndex
            this.bodyColors = new List<Pen>();

            this.bodyColors.Add(new Pen(Brushes.Red, 6));
            this.bodyColors.Add(new Pen(Brushes.Orange, 6));
            this.bodyColors.Add(new Pen(Brushes.Green, 6));
            this.bodyColors.Add(new Pen(Brushes.Blue, 6));
            this.bodyColors.Add(new Pen(Brushes.Indigo, 6));
            this.bodyColors.Add(new Pen(Brushes.Violet, 6));

            // Create the drawing group we'll use for drawing
            this.drawingGroup = new DrawingGroup();

            // Create an image source that we can use in our image control
            this.imageSource = new DrawingImage(this.drawingGroup);
            setDifference();
        }
            
        void setDifference()
        {
            double timestamp = DateTime.Now.ToFileTime();
            /////////////////////////////////////
            // double timestampNTP = networkDateTime.Now.ToFileTime();
            //var networkDateTime = new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds((long)milliseconds);
            //     var networkDateTime = new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds((long)milliseconds);
            var ntpServer = "time.google.com";
            var ntpData = new byte[48];
            ntpData[0] = 0x1B;
            var addresses = Dns.GetHostEntry(ntpServer).AddressList;
            var ipEndPoint = new IPEndPoint(addresses[0], 123);
            var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            socket.Connect(ipEndPoint);
            socket.Send(ntpData);
            socket.Receive(ntpData);
            socket.Close();
            var intPart = (ulong)ntpData[40] << 24 | (ulong)ntpData[41] << 16 | (ulong)ntpData[42] << 8 | (ulong)ntpData[43];
            var fractPart = (ulong)ntpData[44] << 24 | (ulong)ntpData[45] << 16 | (ulong)ntpData[46] << 8 | (ulong)ntpData[47];
            var milliseconds = (intPart * 1000) + ((fractPart * 1000) / 0x100000000L);
            var networkDateTime = new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds((long)milliseconds);
            var localDateTime = networkDateTime.ToLocalTime(); // Türkiye saati için UTC zamanı yerel saate dönüştürün.
            var networkDateTimets = new DateTime(1601, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds((long)milliseconds);
            TimeSpan t = networkDateTime - new DateTime(1970, 1, 1);
            double googleTimeStamp = t.TotalSeconds;

            TimeSpan span = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
            double pcTimeStamp = span.TotalSeconds;

            timeDifference = googleTimeStamp - pcTimeStamp; // Google - PC
            //Console.WriteLine($"Google NTP Time: {networkDateTime.ToString()}");
            //Console.WriteLine($"Google NTP Time: {googleTimeStamp}");
            //Console.WriteLine($"PC Time: {pcTimeStamp}");
            //Console.WriteLine($"Time Difference: {difference}");
        }

        private double getTime()
        {
            TimeSpan span = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
            double pcTimeStamp = span.TotalSeconds;
            return pcTimeStamp + timeDifference;
        }

        /// <summary>
        /// Gets the bitmap to display
        /// </summary>
        public ImageSource ImageSource
        {
            get
            {
                return this.imageSource;
            }
        }

        /// <summary>
        /// Updates the body array with new information from the sensor
        /// Should be called whenever a new BodyFrameArrivedEvent occurs
        /// </summary>
        /// <param name="bodies">Array of bodies to update</param>
        public void UpdateBodyFrame(Body[] bodies)
        {
            if (bodies != null)
            {
                using (DrawingContext dc = this.drawingGroup.Open())
                {
                    // Draw a transparent background to set the render size
                    dc.DrawRectangle(Brushes.Black, null, new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));
                    int indiser1 = -1;
                    int penIndex = 0;
                    List<string> coorname = new List<string>();



                    foreach (Body body in bodies)
                    {
                        indiser1++;
                       // Console.WriteLine("indiser1::" + indiser1);
                        
                        Pen drawPen = this.bodyColors[penIndex++];

                        if (body.IsTracked)
                        {
                            this.DrawClippedEdges(body, dc);

                            IReadOnlyDictionary<JointType, Joint> joints = body.Joints;

                            // convert the joint points to depth (display) space
                            Dictionary<JointType, Point> jointPoints = new Dictionary<JointType, Point>();
                                List<string> coor = new List<string>();
                            List<Object> dataBaseList = new List<object>(); 
                            int indiser2 = 0;

                            //coor.Add(indiser1.ToString()); // adding elements using add() method
                            /*
                            double timestamp = DateTime.Now.ToFileTime();
                            /////////////////////////////////////
                            // double timestampNTP = networkDateTime.Now.ToFileTime();
                            //var networkDateTime = new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds((long)milliseconds);
                            //     var networkDateTime = new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds((long)milliseconds);
                            var ntpServer = "time.google.com";
                            var ntpData = new byte[48];
                            ntpData[0] = 0x1B;
                            var addresses = Dns.GetHostEntry(ntpServer).AddressList;
                            var ipEndPoint = new IPEndPoint(addresses[0], 123);
                            var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                            socket.Connect(ipEndPoint);
                            socket.Send(ntpData);
                            socket.Receive(ntpData);
                            socket.Close();
                            var intPart = (ulong)ntpData[40] << 24 | (ulong)ntpData[41] << 16 | (ulong)ntpData[42] << 8 | (ulong)ntpData[43];
                            var fractPart = (ulong)ntpData[44] << 24 | (ulong)ntpData[45] << 16 | (ulong)ntpData[46] << 8 | (ulong)ntpData[47];
                            var milliseconds = (intPart * 1000) + ((fractPart * 1000) / 0x100000000L);
                            var networkDateTime = new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds((long)milliseconds);
                            var localDateTime = networkDateTime.ToLocalTime(); // Türkiye saati için UTC zamanı yerel saate dönüştürün.
                            var networkDateTimets = new DateTime(1601, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds((long)milliseconds);

                            /////////////////////////////
                            ///
                            TimeSpan t = networkDateTime - new DateTime(1970, 1, 1);
                            double googleTimeStamp = t.TotalSeconds;

                            TimeSpan span = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
                            double pcTimeStamp = span.TotalSeconds;

                            double difference = googleTimeStamp - pcTimeStamp;     // Google - PC
                            Console.WriteLine($"Google NTP Time: {networkDateTime.ToString()}");
                            Console.WriteLine($"Google NTP Time: {googleTimeStamp}");
                            Console.WriteLine($"PC Time: {pcTimeStamp}");
                            Console.WriteLine($"Time Difference: {difference}");


                            Thread.Sleep(1000);

                            span = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
                            pcTimeStamp = span.TotalSeconds;

                            double newGoogleTimeStamp = pcTimeStamp + difference;

                            // Console.WriteLine($"New PC Time: {pcTimeStamp}");
                            // Console.WriteLine($"New Google Time: {newGoogleTimeStamp}");

                            //                            Console.WriteLine($"Google Time Difference: {newGoogleTimeStamp - googleTimeStamp}");
                            //////////////////////////////



                            //                          Console.WriteLine(intPart.ToString());
                            //                          Console.WriteLine(fractPart.ToString());
                            //                          Console.WriteLine(milliseconds.ToString());
                            //                          Console.WriteLine(networkDateTime.ToString());
                            //                          Console.WriteLine(localDateTime.ToString());
                            //                          Console.WriteLine(timestamp.ToString()); 
                            //                          Console.WriteLine(networkDateTimets.ToString());
                            
                            
                            //string networkDateTime = DateTime.Now.ToString("HH:mm:ss tt");

                            //int intPart = 0;
                            //int fractPart = 0;
                            //double timestamp = 0;
                            //double milliseconds = 0.0;*/

                            DateTime dbDt = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                            double tStamp = getTime();
                            dbDt = dbDt.AddSeconds(tStamp).ToLocalTime();

                            dataBaseList.Add(dbDt);
                            
                            var epoch = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
                            dataBaseList.Add(epoch); 



                            // Zamanı kullanarak bir şeyler yapın
                            ////////////////////////////////////
                            foreach (JointType jointType in joints.Keys)
                            {
                                // sometimes the depth(Z) of an inferred joint may show as negative
                                // clamp down to 0.1f to prevent coordinatemapper from returning (-Infinity, -Infinity)
                                CameraSpacePoint position = joints[jointType].Position;
                                //////////////////-----------

                                // x and y in [-1,1], I think
                                CameraSpacePoint cameraPoint = joints[jointType].Position;
                                // x and y in [0, 1080]
                                ColorSpacePoint colorPoint = coordinateMapper.MapCameraPointToColorSpace(cameraPoint);


                               float[] coordinates1 = new float[] { colorPoint.X, colorPoint.Y, 0, 0 }; // 0, 0 is filler for the bounding box
                             

                                /////////////////------------
                                var orientation = body.JointOrientations[JointType.ElbowLeft].Orientation;

                                var rotationX = orientation.Pitch();
                                var rotationY = orientation.Yaw();
                                var rotationZ = orientation.Roll();
                                /////////////////------------
//                                Console.WriteLine("color and rota :: " + coordinates1[0].ToString() + " - " + coordinates1[0].ToString() + " - " +
//                                 rotationX.ToString() + " - " + rotationY.ToString() + " - " + rotationZ.ToString());                                 // Console.WriteLine(" coordinates color space -0        ::" + coordinates[0]);                                / Console.WriteLine(" coordinates color space -0        ::" + coordinates[0]);

                                /////////////////------------


                                if (position.Z < 0)
                                {
                                    position.Z = InferredZPositionClamp;

                                }

                                //Console.WriteLine("indiser1-2        ::" + indiser1+" - "+indiser2);
                                //Console.WriteLine(jointType + " - " + "position.X        :: " + position.X);
                                //Console.WriteLine(jointType + " - " + "position.Y        :: " + position.Y);
                                //Console.WriteLine(jointType + " - " + "position.Z        :: " + position.Z);
                                DepthSpacePoint depthSpacePoint = this.coordinateMapper.MapCameraPointToDepthSpace(position);
                                //Console.WriteLine(jointType + " - " + "depthSpacePoint.X :: " + depthSpacePoint.X);
                                //Console.WriteLine(jointType + " - " + "depthSpacePoint.Y :: " + depthSpacePoint.Y);
                                jointPoints[jointType] = new Point(depthSpacePoint.X, depthSpacePoint.Y);

                                //coor.Add(indiser2.ToString()); // adding elements using add() method
                                coor.Add(position.X.ToString()); // adding elements using add() method
                                coor.Add(position.Y.ToString()); // adding elements using add() method
                                coor.Add(position.Z.ToString()); // adding elements using add() method
                                coor.Add(depthSpacePoint.X.ToString()); // adding elements using add() method
                                coor.Add(depthSpacePoint.Y.ToString()); // adding elements using add() method                                coor.Add(depthSpacePoint.Y.ToString()); // adding elements using add() method
                                coor.Add(colorPoint.X.ToString()); // colorspace x
                                coor.Add(colorPoint.Y.ToString()); // colorspace y
                                coor.Add(rotationX.ToString()); // colorspace y
                                coor.Add(rotationY.ToString()); // colorspace y
                                coor.Add(rotationZ.ToString()); // colorspace y
                                coor.Add(rotationZ.ToString()); // colorspace y

                                dataBaseList.Add(position.Y); // adding elements using add() method
                                dataBaseList.Add(position.X); // adding elements using add() method
                                dataBaseList.Add(position.Z); // adding elements using add() method
                         
                                dataBaseList.Add(depthSpacePoint.X); // adding elements using add() method
                                dataBaseList.Add(depthSpacePoint.Y); // adding elements using add() method                                coor.Add(depthSpacePoint.Y.ToString()); // adding elements using add() method
                                dataBaseList.Add(colorPoint.X); // colorspace x
                                dataBaseList.Add(colorPoint.Y); // colorspace y
                                dataBaseList.Add(rotationX); // colorspace y
                                dataBaseList.Add(rotationY); // colorspace y
                                dataBaseList.Add(rotationZ); // colorspace y
                                dataBaseList.Add(rotationZ); // colorspace y

                                coorname.Add(jointType.ToString()); // adding elements using add() method

                                indiser2++;

                                





                            }

                            //Infinity fix

                            for(int i = 0; i < dataBaseList.Count; i++)
                            {
                                if (dataBaseList[i].GetType() == typeof(float) && float.IsInfinity((float)dataBaseList[i]))
                                {
                                    dataBaseList[i] = -1000;
                                }
                            } 
                             
                            string connectionString = "Data Source=193.35.200.106;Initial Catalog=Prevla;User ID=SA;Password=hP337^9nArG&;";
                            using (SqlConnection connection = new SqlConnection(connectionString))
                            {
                                connection.Open();
                                Console.WriteLine("Sa");
                                string tableName = "KinectDatas";
                                string[] columns = new string[] { "kinect_date", "kinect_timestamp", "kinect_7_x_pos_SpineBase", "kinect_8_y_pos_SpineBase", "kinect_9_z_pos_SpineBase", "kinect_10_x_depth_SpineBase", "kinect_11_y_depth_SpineBase", "kinect_12_x_color_SpineBase", "kinect_13_y_color_SpineBase", "kinect_14_x_orientation_SpineBase", "kinect_15_y_orientation_SpineBase", "kinect_16_z_orientation_SpineBase", "kinect_17_w_orientation_SpineBase", "kinect_18_x_pos_SpineMid", "kinect_19_y_pos_SpineMid", "kinect_20_z_pos_SpineMid", "kinect_21_x_depth_SpineMid", "kinect_22_y_depth_SpineMid", "kinect_23_x_color_SpineMid", "kinect_24_y_color_SpineMid", "kinect_25_x_orientation_SpineMid", "kinect_26_y_orientation_SpineMid", "kinect_27_z_orientation_SpineMid", "kinect_28_w_orientation_SpineMid", "kinect_29_x_pos_Neck", "kinect_30_y_pos_Neck", "kinect_31_z_pos_Neck", "kinect_32_x_depth_Neck", "kinect_33_y_depth_Neck", "kinect_34_x_color_Neck", "kinect_35_y_color_Neck", "kinect_36_x_orientation_Neck", "kinect_37_y_orientation_Neck", "kinect_38_z_orientation_Neck", "kinect_39_w_orientation_Neck", "kinect_40_x_pos_Head", "kinect_41_y_pos_Head", "kinect_42_z_pos_Head", "kinect_43_x_depth_Head", "kinect_44_y_depth_Head", "kinect_45_x_color_Head", "kinect_46_y_color_Head", "kinect_47_x_orientation_Head", "kinect_48_y_orientation_Head", "kinect_49_z_orientation_Head", "kinect_50_w_orientation_Head", "kinect_51_x_pos_ShoulderLeft", "kinect_52_y_pos_ShoulderLeft", "kinect_53_z_pos_ShoulderLeft", "kinect_54_x_depth_ShoulderLeft", "kinect_55_y_depth_ShoulderLeft", "kinect_56_x_color_ShoulderLeft", "kinect_57_y_color_ShoulderLeft", "kinect_58_x_orientation_ShoulderLeft", "kinect_59_y_orientation_ShoulderLeft", "kinect_60_z_orientation_ShoulderLeft", "kinect_61_w_orientation_ShoulderLeft", "kinect_62_x_pos_ElbowLeft", "kinect_63_y_pos_ElbowLeft", "kinect_64_z_pos_ElbowLeft", "kinect_65_x_depth_ElbowLeft", "kinect_66_y_depth_ElbowLeft", "kinect_67_x_color_ElbowLeft", "kinect_68_y_color_ElbowLeft", "kinect_69_x_orientation_ElbowLeft", "kinect_70_y_orientation_ElbowLeft", "kinect_71_z_orientation_ElbowLeft", "kinect_72_w_orientation_ElbowLeft", "kinect_73_x_pos_WristLeft", "kinect_74_y_pos_WristLeft", "kinect_75_z_pos_WristLeft", "kinect_76_x_depth_WristLeft", "kinect_77_y_depth_WristLeft", "kinect_78_x_color_WristLeft", "kinect_79_y_color_WristLeft", "kinect_80_x_orientation_WristLeft", "kinect_81_y_orientation_WristLeft", "kinect_82_z_orientation_WristLeft", "kinect_83_w_orientation_WristLeft", "kinect_84_x_pos_HandLeft", "kinect_85_y_pos_HandLeft", "kinect_86_z_pos_HandLeft", "kinect_87_x_depth_HandLeft", "kinect_88_y_depth_HandLeft", "kinect_89_x_color_HandLeft", "kinect_90_y_color_HandLeft", "kinect_91_x_orientation_HandLeft", "kinect_92_y_orientation_HandLeft", "kinect_93_z_orientation_HandLeft", "kinect_94_w_orientation_HandLeft", "kinect_95_x_pos_ShoulderRight", "kinect_96_y_pos_ShoulderRight", "kinect_97_z_pos_ShoulderRight", "kinect_98_x_depth_ShoulderRight", "kinect_99_y_depth_ShoulderRight", "kinect_100_x_color_ShoulderRight", "kinect_101_y_color_ShoulderRight", "kinect_102_x_orientation_ShoulderRight", "kinect_103_y_orientation_ShoulderRight", "kinect_104_z_orientation_ShoulderRight", "kinect_105_w_orientation_ShoulderRight", "kinect_106_x_pos_ElbowRight", "kinect_107_y_pos_ElbowRight", "kinect_108_z_pos_ElbowRight", "kinect_109_x_depth_ElbowRight", "kinect_110_y_depth_ElbowRight", "kinect_111_x_color_ElbowRight", "kinect_112_y_color_ElbowRight", "kinect_113_x_orientation_ElbowRight", "kinect_114_y_orientation_ElbowRight", "kinect_115_z_orientation_ElbowRight", "kinect_116_w_orientation_ElbowRight", "kinect_117_x_pos_WristRight", "kinect_118_y_pos_WristRight", "kinect_119_z_pos_WristRight", "kinect_120_x_depth_WristRight", "kinect_121_y_depth_WristRight", "kinect_122_x_color_WristRight", "kinect_123_y_color_WristRight", "kinect_124_x_orientation_WristRight", "kinect_125_y_orientation_WristRight", "kinect_126_z_orientation_WristRight", "kinect_127_w_orientation_WristRight", "kinect_128_x_pos_HandRight", "kinect_129_y_pos_HandRight", "kinect_130_z_pos_HandRight", "kinect_131_x_depth_HandRight", "kinect_132_y_depth_HandRight", "kinect_133_x_color_HandRight", "kinect_134_y_color_HandRight", "kinect_135_x_orientation_HandRight", "kinect_136_y_orientation_HandRight", "kinect_137_z_orientation_HandRight", "kinect_138_w_orientation_HandRight", "kinect_139_x_pos_HipLeft", "kinect_140_y_pos_HipLeft", "kinect_141_z_pos_HipLeft", "kinect_142_x_depth_HipLeft", "kinect_143_y_depth_HipLeft", "kinect_144_x_color_HipLeft", "kinect_145_y_color_HipLeft", "kinect_146_x_orientation_HipLeft", "kinect_147_y_orientation_HipLeft", "kinect_148_z_orientation_HipLeft", "kinect_149_w_orientation_HipLeft", "kinect_150_x_pos_KneeLeft", "kinect_151_y_pos_KneeLeft", "kinect_152_z_pos_KneeLeft", "kinect_153_x_depth_KneeLeft", "kinect_154_y_depth_KneeLeft", "kinect_155_x_color_KneeLeft", "kinect_156_y_color_KneeLeft", "kinect_157_x_orientation_KneeLeft", "kinect_158_y_orientation_KneeLeft", "kinect_159_z_orientation_KneeLeft", "kinect_160_w_orientation_KneeLeft", "kinect_161_x_pos_AnkleLeft", "kinect_162_y_pos_AnkleLeft", "kinect_163_z_pos_AnkleLeft", "kinect_164_x_depth_AnkleLeft", "kinect_165_y_depth_AnkleLeft", "kinect_166_x_color_AnkleLeft", "kinect_167_y_color_AnkleLeft", "kinect_168_x_orientation_AnkleLeft", "kinect_169_y_orientation_AnkleLeft", "kinect_170_z_orientation_AnkleLeft", "kinect_171_w_orientation_AnkleLeft", "kinect_172_x_pos_FootLeft", "kinect_173_y_pos_FootLeft", "kinect_174_z_pos_FootLeft", "kinect_175_x_depth_FootLeft", "kinect_176_y_depth_FootLeft", "kinect_177_x_color_FootLeft", "kinect_178_y_color_FootLeft", "kinect_179_x_orientation_FootLeft", "kinect_180_y_orientation_FootLeft", "kinect_181_z_orientation_FootLeft", "kinect_182_w_orientation_FootLeft", "kinect_183_x_pos_HipRight", "kinect_184_y_pos_HipRight", "kinect_185_z_pos_HipRight", "kinect_186_x_depth_HipRight", "kinect_187_y_depth_HipRight", "kinect_188_x_color_HipRight", "kinect_189_y_color_HipRight", "kinect_190_x_orientation_HipRight", "kinect_191_y_orientation_HipRight", "kinect_192_z_orientation_HipRight", "kinect_193_w_orientation_HipRight", "kinect_194_x_pos_KneeRight", "kinect_195_y_pos_KneeRight", "kinect_196_z_pos_KneeRight", "kinect_197_x_depth_KneeRight", "kinect_198_y_depth_KneeRight", "kinect_199_x_color_KneeRight", "kinect_200_y_color_KneeRight", "kinect_201_x_orientation_KneeRight", "kinect_202_y_orientation_KneeRight", "kinect_203_z_orientation_KneeRight", "kinect_204_w_orientation_KneeRight", "kinect_205_x_pos_AnkleRight", "kinect_206_y_pos_AnkleRight", "kinect_207_z_pos_AnkleRight", "kinect_208_x_depth_AnkleRight", "kinect_209_y_depth_AnkleRight", "kinect_210_x_color_AnkleRight", "kinect_211_y_color_AnkleRight", "kinect_212_x_orientation_AnkleRight", "kinect_213_y_orientation_AnkleRight", "kinect_214_z_orientation_AnkleRight", "kinect_215_w_orientation_AnkleRight", "kinect_216_x_pos_FootRight", "kinect_217_y_pos_FootRight", "kinect_218_z_pos_FootRight", "kinect_219_x_depth_FootRight", "kinect_220_y_depth_FootRight", "kinect_221_x_color_FootRight", "kinect_222_y_color_FootRight", "kinect_223_x_orientation_FootRight", "kinect_224_y_orientation_FootRight", "kinect_225_z_orientation_FootRight", "kinect_226_w_orientation_FootRight", "kinect_227_x_pos_SpineShoulder", "kinect_228_y_pos_SpineShoulder", "kinect_229_z_pos_SpineShoulder", "kinect_230_x_depth_SpineShoulder", "kinect_231_y_depth_SpineShoulder", "kinect_232_x_color_SpineShoulder", "kinect_233_y_color_SpineShoulder", "kinect_234_x_orientation_SpineShoulder", "kinect_235_y_orientation_SpineShoulder", "kinect_236_z_orientation_SpineShoulder", "kinect_237_w_orientation_SpineShoulder", "kinect_238_x_pos_HandTipLeft", "kinect_239_y_pos_HandTipLeft", "kinect_240_z_pos_HandTipLeft", "kinect_241_x_depth_HandTipLeft", "kinect_242_y_depth_HandTipLeft", "kinect_243_x_color_HandTipLeft", "kinect_244_y_color_HandTipLeft", "kinect_245_x_orientation_HandTipLeft", "kinect_246_y_orientation_HandTipLeft", "kinect_247_z_orientation_HandTipLeft", "kinect_248_w_orientation_HandTipLeft", "kinect_249_x_pos_ThumbLeft", "kinect_250_y_pos_ThumbLeft", "kinect_251_z_pos_ThumbLeft", "kinect_252_x_depth_ThumbLeft", "kinect_253_y_depth_ThumbLeft", "kinect_254_x_color_ThumbLeft", "kinect_255_y_color_ThumbLeft", "kinect_256_x_orientation_ThumbLeft", "kinect_257_y_orientation_ThumbLeft", "kinect_258_z_orientation_ThumbLeft", "kinect_259_w_orientation_ThumbLeft", "kinect_260_x_pos_HandTipRight", "kinect_261_y_pos_HandTipRight", "kinect_262_z_pos_HandTipRight", "kinect_263_x_depth_HandTipRight", "kinect_264_y_depth_HandTipRight", "kinect_265_x_color_HandTipRight", "kinect_266_y_color_HandTipRight", "kinect_267_x_orientation_HandTipRight", "kinect_268_y_orientation_HandTipRight", "kinect_269_z_orientation_HandTipRight", "kinect_270_w_orientation_ThumbRight", "kinect_271_x_pos_ThumbRight", "kinect_272_y_pos_ThumbRight", "kinect_273_z_pos_ThumbRight", "kinect_274_x_depth_ThumbRight", "kinect_275_y_depth_ThumbRight", "kinect_276_x_color_ThumbRight", "kinect_277_y_color_ThumbRight", "kinect_278_x_orientation_ThumbRight", "kinect_279_y_orientation_ThumbRight", "kinect_280_z_orientation_ThumbRight", "kinect_281_w_orientation_ThumbRight" };
                                //object[] parameters = new object[] { "testname", 1.0f, 2.0f, 3.0f };
                                
                                string sql = $"INSERT INTO {tableName} ({string.Join(", ", columns)}) VALUES ({string.Join(", ", dataBaseList.Select((_, i) => $"@{i}"))})";

                                using (SqlCommand command = new SqlCommand(sql, connection))
                                {
                                    for (int i = 0; i < dataBaseList.Count; i++)
                                    {
                                        SqlParameter parameter = new SqlParameter($"@{i}", GetSqlDbType(dataBaseList[i],i));
                                        parameter.Value = dataBaseList[i];
                                        command.Parameters.Add(parameter);
                                    }

                                  int rowsAffected = command.ExecuteNonQuery();
                                    Console.WriteLine(rowsAffected + " rows inserted.");
                                }
                                connection.Close();
                            }
                            



                            //Console.WriteLine(value: ColorSpacePoint.ToString());


                            //Console.WriteLine();
                            // var epoch = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
                            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                            double timestamp = getTime();
                            dt = dt.AddSeconds(timestamp).ToLocalTime();

                            string currentDirectory = Directory.GetCurrentDirectory();

                            DirectoryInfo parentDirectory = Directory.GetParent(currentDirectory);
                            for(int i = 0; i < 3; i++)
                            {
                                parentDirectory = Directory.GetParent(parentDirectory.ToString());
                            }

                            
                            string fileName = parentDirectory.FullName + "\\datacollection\\kinect_dataseter.csv";

                            using (FileStream fs = new FileStream(fileName, FileMode.Append, FileAccess.Write))
                            {
                                using (StreamWriter sw = new StreamWriter(fs))
                                {

                                    sw.Write(dt);sw.Write(";");
                                    sw.Write(timestamp.ToString()); sw.Write(";");
                                    
                                    foreach (var tt in coor)
                                    {
                                        sw.Write(tt);
                                        sw.Write(";");

                                    }
                                    sw.Write("\n");



                                    //TODO
                                    //burada time stamp 6 kolondan sonra coor list
                                    // burasi database'e gidecek kısım





                                }
                            }





                            this.DrawBody(joints, jointPoints, dc, drawPen);

                            this.DrawHand(body.HandLeftState, jointPoints[JointType.HandLeft], dc);
                            this.DrawHand(body.HandRightState, jointPoints[JointType.HandRight], dc);
                        }
                    }

                    // prevent drawing outside of our render area
                    this.drawingGroup.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));
                }
            }
        }

        static SqlDbType GetSqlDbType(object value,int i)
        {
            if (value is string) return SqlDbType.VarChar;
            else if (value is int) return SqlDbType.Int;
            else if (value is float) return SqlDbType.Float;
            else if (value is double) return SqlDbType.Float;
            else if (value is decimal) return SqlDbType.Decimal;
            else if (value is DateTime) return SqlDbType.DateTime;
            else if (value is DateTime) return SqlDbType.DateTime2;
            else if (value is bool) return SqlDbType.Bit;
            else if (value is long) return SqlDbType.BigInt;
            else if (value is ulong) return SqlDbType.BigInt;

            else throw new ArgumentException("Unknown data type: " + value.GetType()+ " "+ value);
        }


        /// <summary>
        /// Draws a body
        /// </summary>
        /// <param name="joints">joints to draw</param>
        /// <param name="jointPoints">translated positions of joints to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        /// <param name="drawingPen">specifies color to draw a specific body</param>
        private void DrawBody(IReadOnlyDictionary<JointType, Joint> joints, IDictionary<JointType, Point> jointPoints, DrawingContext drawingContext, Pen drawingPen)
        {
            // Draw the bones
            foreach (var bone in this.bones)
            {
                this.DrawBone(joints, jointPoints, bone.Item1, bone.Item2, drawingContext, drawingPen);
            }

            // Draw the joints
            foreach (JointType jointType in joints.Keys)
            {
                Brush drawBrush = null;

                TrackingState trackingState = joints[jointType].TrackingState;

                if (trackingState == TrackingState.Tracked)
                {
                    drawBrush = this.trackedJointBrush;
                }
                else if (trackingState == TrackingState.Inferred)
                {
                    drawBrush = this.inferredJointBrush;
                }

                if (drawBrush != null)
                {
                    drawingContext.DrawEllipse(drawBrush, null, jointPoints[jointType], JointThickness, JointThickness);
                }
            }
        }

        /// <summary>
        /// Draws one bone of a body (joint to joint)
        /// </summary>
        /// <param name="joints">joints to draw</param>
        /// <param name="jointPoints">translated positions of joints to draw</param>
        /// <param name="jointType0">first joint of bone to draw</param>
        /// <param name="jointType1">second joint of bone to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        /// /// <param name="drawingPen">specifies color to draw a specific bone</param>
        private void DrawBone(IReadOnlyDictionary<JointType, Joint> joints, IDictionary<JointType, Point> jointPoints, JointType jointType0, JointType jointType1, DrawingContext drawingContext, Pen drawingPen)
        {
            Joint joint0 = joints[jointType0];
            Joint joint1 = joints[jointType1];

            // If we can't find either of these joints, exit
            if (joint0.TrackingState == TrackingState.NotTracked ||
                joint1.TrackingState == TrackingState.NotTracked)
            {
                return;
            }

            // We assume all drawn bones are inferred unless BOTH joints are tracked
            Pen drawPen = this.inferredBonePen;
            if ((joint0.TrackingState == TrackingState.Tracked) && (joint1.TrackingState == TrackingState.Tracked))
            {
                drawPen = drawingPen;
            }

            drawingContext.DrawLine(drawPen, jointPoints[jointType0], jointPoints[jointType1]);
        }

        /// <summary>
        /// Draws a hand symbol if the hand is tracked: red circle = closed, green circle = opened; blue circle = lasso
        /// </summary>
        /// <param name="handState">state of the hand</param>
        /// <param name="handPosition">position of the hand</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawHand(HandState handState, Point handPosition, DrawingContext drawingContext)
        {
            switch (handState)
            {
                case HandState.Closed:
                    drawingContext.DrawEllipse(this.handClosedBrush, null, handPosition, HandSize, HandSize);
                    break;

                case HandState.Open:
                    drawingContext.DrawEllipse(this.handOpenBrush, null, handPosition, HandSize, HandSize);
                    break;

                case HandState.Lasso:
                    drawingContext.DrawEllipse(this.handLassoBrush, null, handPosition, HandSize, HandSize);
                    break;
            }
        }

        /// <summary>
        /// Draws indicators to show which edges are clipping body data
        /// </summary>
        /// <param name="body">body to draw clipping information for</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawClippedEdges(Body body, DrawingContext drawingContext)
        {
            FrameEdges clippedEdges = body.ClippedEdges;

            if (clippedEdges.HasFlag(FrameEdges.Bottom))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, this.displayHeight - ClipBoundsThickness, this.displayWidth, ClipBoundsThickness));
            }

            if (clippedEdges.HasFlag(FrameEdges.Top))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, this.displayWidth, ClipBoundsThickness));
            }

            if (clippedEdges.HasFlag(FrameEdges.Left))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, ClipBoundsThickness, this.displayHeight));
            }

            if (clippedEdges.HasFlag(FrameEdges.Right))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(this.displayWidth - ClipBoundsThickness, 0, ClipBoundsThickness, this.displayHeight));
            }
        }
    }
}
